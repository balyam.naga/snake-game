import tkinter as tk
from PIL import Image, ImageTk
from random import randint

class Snake(tk.Canvas):

    def __init__(self):
        super().__init__(width=600,height=630,background="black",highlightthickness=0)

        self.snake_positions = [(100,100), (80,100), (60,100)]
        self.food_position = self.set_new_food_position()
        self.load_assets()
        self.create_objects()
        self.after(75,self.perform_actions)
        self.direction = "Right"
        self.bind_all("<Key>",self.on_key_press)
        self.start_pointer = 0
        self.game_speed = 75
    #loading the assets
    def load_assets(self):
            self.snake_body_image = Image.open('./assets/snake1.png')
            self.snake_body = ImageTk.PhotoImage(self.snake_body_image)
            self.food_image = Image.open('./assets/food1.png')
            self.snake_food = ImageTk.PhotoImage(self.food_image)
            self.score = 0

    def create_objects(self):
        self.create_text(
            45,12, text= f"Score: {self.score}", tag="score", fill="#fff", font=("TkDefaultFont",14)
        )

        self.create_rectangle(7,27,593,613,outline="#525d69")

        for x_position,y_position in self.snake_positions:
            self.create_image(x_position, y_position,image=self.snake_body,tag="snake")

        self.create_image(*self.food_position,image=self.snake_food,tag="food")

    def move_snake(self):
        head_x_position,head_y_position = self.snake_positions[0]
        if self.direction == "Left":
            new_head_position = (head_x_position - 20, head_y_position)
        elif self.direction == "Right":
            new_head_position = (head_x_position + 20, head_y_position)
        elif self.direction == "Up":
            new_head_position = (head_x_position, head_y_position - 20)
        elif self.direction == "Down":
            new_head_position = (head_x_position, head_y_position + 20)

        self.snake_positions = [new_head_position]+self.snake_positions[:-1]

        for segment,position in zip(self.find_withtag("snake"),self.snake_positions):
            self.coords(segment,position)

    def perform_actions(self):
        if self.check_collision():
            self.end_game()
            return

        self.food_collision()
        self.move_snake()
        if self.score > self.start_pointer + 2:
            self.start_pointer = self.score
            self.game_speed = self.game_speed - 10
        self.after(self.game_speed,self.perform_actions)

    def check_collision(self):
        head_x_position,head_y_position = self.snake_positions[0]
        return(
            head_x_position in (0,600) or head_y_position in (40,620) or (head_x_position,head_y_position) in self.snake_positions[1:]
        )

    def on_key_press(self, e):
        new_direction = e.keysym
        all_keys = ['Up' ,'Down', 'Left', 'Right']
        opps = {'Up': 'Down', 'Down' : 'Up', 'Left': 'Right', 'Right': 'Left'}
        if new_direction in all_keys:
            if opps[self.direction] != new_direction:
                self.direction = new_direction

    def food_collision(self):
        if self.snake_positions[0] == self.food_position:
            self.score += 1
            self.snake_positions.append(self.snake_positions[-1])

            self.create_image( *self.snake_positions[-1], image=self.snake_body, tag="snake" )

            self.food_position = self.set_new_food_position()
            self.coords(self.find_withtag("food"),self.food_position)
            score = self.find_withtag("score")
            self.itemconfigure(score, text = f"Score: {self.score}", tag="score")

    def set_new_food_position(self):
        while True:
            x_position = randint(1, 29) * 20
            y_position = randint(3, 30) * 20

            food_position = (x_position, y_position)

            if food_position not in self.snake_positions:
                return food_position

    def end_game(self):
        self.delete(tk.ALL)
        self.create_text(
            self.winfo_width() / 2,
            self.winfo_height() / 2,
            text = f"Game Over! You scored {self.score}!",
            fill="#fff",
            font=24
        )

root = tk.Tk()
root.title("Snake")
root.resizable(False,False)


board = Snake()
board.pack()

root.mainloop()